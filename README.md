# testmirroh
 Để điều tiết dòng tiền khi thị trường giảm tốc, các công ty bất động sản tính chuyện rút vốn khỏi công ty con.

Công ty cổ phần Quốc Cường Gia Lai là một trong những doanh nghiệp có mục tiêu rõ ràng nhất khi quyết định thoái hết vốn tại doanh nghiệp thành viên. Hồi tháng 10/2019, QCG công bố Nghị quyết HĐQT chuyển nhượng toàn bộ 18,6% vốn tại Công ty Chánh Nghĩa Quốc Cường với giá 132 tỷ đồng.

Chánh Nghĩa Quốc Cường là công ty thành viên của QCG, thành lập ngày 25/9/2018 do ông Nguyễn Quốc Cường làm Tổng giám đốc kiêm người đại diện theo pháp luật. Chủ tịch Hội đồng quản trị QCG, bà Nguyễn Thị Như Loan sẽ trực tiếp tìm kiếm đối tác và đàm phán ký hợp đồng chuyển nhượng số cổ phần này.

Bà Loan cho biết quyết định bán toàn bộ số cổ phần này vì áp lực dòng tiền để trả nợ cá nhân mà công ty đã mượn. Hiện các dự án của QCG tại TP HCM đang bị đình trệ do thủ tục pháp lý chậm trễ trong khâu giao đất và đây cũng là tình trạng chung của tất cả các dự án trên địa bàn thành phố.

Trong khi đó, nếu tiếp tục theo đuổi dự án của công ty thành viên tại Bình Dương thì QCG phải rót thêm tiền vào để thực hiện theo tỷ lệ 18,6%. Điều này sẽ làm tăng thêm áp lực về dòng tiền. Mặt khác việc vay vốn đầu tư 